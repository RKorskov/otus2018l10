-*- mode: markdown; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
-*- eval: (set-language-environment Russian) -*-  
--- Time-stamp: <2019-05-14 17:58:58 korskov>

Overview
========

otus-java_2018-10

Группа 2018-10  
Студент:  
Roman Korskov (Роман Корсков)  
korskov.r.v@gmail.com

[https://otus.ru/lessons/razrabotchik-java/][Разработчик Java  
Курс об особенностях языка и платформы Java, стандартной библиотеке, о проектировании и тестировании, о том, как работать с базами, файлами, веб-фронтендом и другими приложениями]

Домашние Работы
===============

# L10 Самодельный ORM

Работа должна использовать базу данных H2.  
Создайте в базе таблицу User с полями:

* id bigint(20) NOT NULL auto_increment
* name varchar(255)
* age int(3)

Создайте свою аннотацию @Id

Создайте класс User (с полями, которые соответствуют таблице, поле id отметьте аннотацией).

Напишите Executor, который умеет работать с классами, в котрых есть поле с аннотацией @Id.  
Executor должен сохранять объект в базу и читать объект из базы.  
Имя таблицы должно соответствовать имени класса, а поля класса - это колонки в таблице.

Методы Executor'а:  
void save(T objectData);  
<T> T load(long id, Class<T> clazz);

Проверьте его работу на классе User.

Комментарии к реализации Executor:  
Метод save.  
С помощью рефлексии в объекте objectData надо найти поле, отмеченное @Id  
По значению поля @Id надо проверить, если ли этот объект в базе или нет.  
Если объекта нет, надо сформировать insert, если есть update.

Замечания
=========

1. В java классы принято раскладывать по пакетам

2. Connection h2conn = h2inst.open();
лучше в try-c-ресурсами положить, чтобы коннект "сам закрывался".

3. interface Enuntiatio 
если что-то даже временно не используется, то это лучше удалить.
Это же и к некоторым другим классам относится.

4. class Executor
boolean UF = false;
в Java переменные с "маленькой буквы"

5. for (Annotation fa: fas) {
            String an = fa.toString();
            if (an.matches("^@(.+\\.)?Id\\(\\)$")) // may be better...
                return true;
        }
Поищите вариант, как искать аннотацию по типу, а не парсить строку.

6. String qstr = String.format
            ("select `%2$s` from `%1$s` where `%2$s`='%3$s' limit 1;",
             tableName, keyName, keyValue);
...
"select * from `%1$s` where `%2$s`='%3$s';",
Запросы нельзя конкатенировать, их надо параметризовать через preparedStatement

7. class UserH2 
Кажется, имя класса не самое удачное.
И тоже конкатенация запросов.

8. не забывайте закрывать ResultSet rs

