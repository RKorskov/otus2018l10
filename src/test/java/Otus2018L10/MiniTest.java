// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-02-06 19:00:23 korskov>

package Otus2018L10;

/** L10 Самодельный ORM
 * Работа должна использовать базу данных H2.
 * Создайте в базе таблицу User с полями:
 *
 * 1. id bigint(20) NOT NULL auto_increment
 * 2. name varchar(255)
 * 3. age int(3)
 *
 * Создайте свою аннотацию @Id
 *
 * Создайте класс User (с полями, которые соответствуют таблице, поле
 * id отметьте аннотацией).
 *
 * Напишите Executor, который умеет работать с классами, в котрых есть поле с аннотацией @Id.
 * Executor должен сохранять объект в базу и читать объект из базы.
 * Имя таблицы должно соответствовать имени класса, а поля класса - это колонки в таблице.
 *
 * Методы Executor'а:  
 * void save(T objectData);  
 * <T> T load(long id, Class<T> clazz);
 *
 * Проверьте его работу на классе User.
 *
 * Комментарии к реализации Executor:  
 * 1. Метод save.  
 * С помощью рефлексии в объекте objectData надо найти поле, отмеченное @Id .
 * По значению поля @Id надо проверить, если ли этот объект в базе или нет.
 * Если объекта нет, надо сформировать insert, если есть update.
 */

/*
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
*/

/**
 * Unit test for simple App.
 */

public class MiniTest {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    //public AppTest (String testName) {super (testName);}

    /**
     * @return the suite of tests being tested
     */
    //public static Test suite() {return new TestSuite (AppTest.class);}

    /**
     * Rigourous Test :-)
     */
    //public void testApp() {assertTrue (true);}
}
