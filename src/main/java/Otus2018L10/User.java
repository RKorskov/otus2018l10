// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-02-14 15:27:52 korskov>

package Otus2018L10;

public class User {
    //final String DEFAULT_FORMAT = "id:%d 名子:%s 岁数:%d";

    @Id
    final long id;
    String name;
    int age;

    private static class SingletonHolder {
        public static final SingletonHolder counterInstance = new SingletonHolder();
        private long counter;
        public static long next() {
            return ++counterInstance.counter;
        }
    }

    User() {
        id = SingletonHolder.next();
        name = null;
        age = -1;
    }

    User (long id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    User (String name, int age) {
        this.id = SingletonHolder.next();
        this.name = name;
        this.age = age;
    }

    public long getId() {return id;}
    public int getAge() {return age;}
    public String getName() {return name;}

    public long setAge (final int reAge) {
        int a = age;
        if (reAge >= 0)
            age = reAge;
        return a;
    }

    public String setName (final String reName) {
        String n = name;
        if (reName != null && reName.length() > 0)
            name = reName;
        return n;
    }

    public String toString() {
        return String.format ("id:%d 名子:%s 岁数:%d", id, name, age);
    }
}
