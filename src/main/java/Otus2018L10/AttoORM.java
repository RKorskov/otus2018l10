// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-03-07 14:56:39 korskov>

package Otus2018L10;

/** L10 Самодельный ORM
 * Работа должна использовать базу данных H2.
 * Создайте в базе таблицу User с полями:
 *
 * 1. id bigint(20) NOT NULL auto_increment
 * 2. name varchar(255)
 * 3. age int(3)
 *
 * Создайте свою аннотацию @Id
 *
 * Создайте класс User (с полями, которые соответствуют таблице, поле
 * id отметьте аннотацией).
 *
 * Напишите Executor, который умеет работать с классами, в котрых есть поле с аннотацией @Id.
 * Executor должен сохранять объект в базу и читать объект из базы.
 * Имя таблицы должно соответствовать имени класса, а поля класса - это колонки в таблице.
 *
 * Методы Executor'а:  
 * void save(T objectData);  
 * <T> T load(long id, Class<T> clazz);
 *
 * Проверьте его работу на классе User.
 *
 * Комментарии к реализации Executor:  
 * 1. Метод save.  
 * С помощью рефлексии в объекте objectData надо найти поле, отмеченное @Id .
 * По значению поля @Id надо проверить, если ли этот объект в базе или нет.
 * Если объекта нет, надо сформировать insert, если есть update.

 * https://stackoverflow.com/questions/19154202/data-access-object-dao-in-java#19154487
 * http://zetcode.com/java/h2database/
 */

import java.lang.StringBuffer;

import java.util.List;
import java.util.ArrayList;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.h2.tools.Server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AttoORM {
    static final Logger logger = LoggerFactory.getLogger (AttoORM.class);

    // http://zetcode.com/java/h2database/
    final static String H2_URL = "jdbc:h2:tcp://localhost/tmp/h2test",
        H2_USER = "h2root";

    public static void main (final String[] args) throws Exception {
        System.out.println ("AttoORM: Hello World!");
        new AttoORM().exec(args);
    }

    private void exec (final String[] args) {
        try (H2Connect h2inst = new H2Connect();
             Connection h2conn = h2inst.open()) {
            // H2Connect h2inst = new H2Connect();
            // Connection h2conn = h2inst.open();
            doSomething (h2conn);
            // h2conn.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void doSomething (Connection h2conn) {
        User lu, so = null;
        Executor xe = new Executor (h2conn);
        if (false) {
            UserH2 uh2 = new UserH2();
            uh2.insert (new User ("LUser", 7));
            uh2.insert (new User ("SilverAge", 18));
            uh2.insert (new User ("GoldenAge", 28));

            List <User> lou;
            lou = uh2.findById (lu.getId());
            so = lou != null && lou.size() > 0 ? lou.get(0) : null;
        }
        else {
            xe.createNewDBTable();
            ArrayList <User> usrs = new ArrayList <> ();
            addUser (xe, new User ("LUser", 7), usrs);
            addUser (xe, new User ("el nino", 6), usrs);
            addUser (xe, new User ("youngster", 28), usrs);
            addUser (xe, new User ("Lich", 496), usrs);
            for (User u : usrs)
                System.out.println (u);
            for (User u : usrs)
                System.out.println (xe.load (u.getId(), User.class));
            for (User u : usrs) {
                u.setAge (u.getAge() * 13/9);
                xe.save (u);
            }
            for (User u : usrs)
                System.out.println (u);
            for (User u : usrs)
                System.out.println (xe.load (u.getId(), User.class));
        }
    }

    private void addUser (Executor xe, User u, ArrayList <User> usrs) {
        xe.save (u);
        usrs.add(u);
    }

}
