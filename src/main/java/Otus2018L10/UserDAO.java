// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-02-13 18:39:46 korskov>

/**
 * https://stackoverflow.com/questions/19154202/data-access-object-dao-in-java#19154487
 */

package Otus2018L10;

import java.util.List;

interface UserDAO {
    public List <User> findAll();
    public List <User> findById (final long uid); // a 1-item list, but still the list
    public List <User> findByName (final String uname);
    public List <User> findByAge (final long uage);
    public boolean insert (final User user);
    public boolean update (final User user);
    /* NI
     * public boolean delete (User user);
     */
}
