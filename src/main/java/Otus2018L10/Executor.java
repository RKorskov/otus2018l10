// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-05-14 19:20:56 korskov>

package Otus2018L10;

import java.lang.AutoCloseable;
import java.lang.Long;
import java.lang.Integer;
import java.lang.IllegalAccessException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import java.lang.annotation.*;
import java.lang.reflect.*;
import java.lang.Class;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import org.h2.tools.Server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * https://dev.mysql.com/doc/refman/8.0/en/create-table.html
 * https://dev.mysql.com/doc/refman/8.0/en/insert.html
 * https://dev.mysql.com/doc/refman/8.0/en/update.html
 * https://dev.mysql.com/doc/refman/8.0/en/select.html
 */

class Executor implements AutoCloseable {
    static final Logger logger = LoggerFactory.getLogger (Executor.class);

    // оставить до переработки в PreparedStatement
    final static boolean __USE_SQL_STRING = false;

    static final String createTableStatementStr = "create table if not exists `User` (`id` bigint(20) not null auto_increment primary key, `name` varchar(255), `age` int(3))",
        createDBStatementStr = "create database if not exists `Otus2018L10`",
        useDBStr = "use ?",
        selectNameValueLim1Str = "select `%2$s` from `%1$s` where `%2$s`=? limit 1",
        selectNameValueStr = "select * from `%s` where `%s`=?";

    private Connection dbconn;
    private Statement dbst;
    // private PreparedStatement createDBStatement, createTableStatement, selectNameValue, selectNameValueLim1;

    /** http://www.h2database.com/html/quickstart.html
        http://zetcode.com/java/h2database/
     */
    //Executor() {;}
    Executor (Connection newDBConn) {
        this.dbconn = newDBConn;
        dbst = null;
        // createDBStatement = this.dbconn.prepareStatement (createDBStatementStr);
        // createTableStatement = this.dbconn.prepareStatement (createTableStatementStr);
        // useDB = this.dbconn.prepareStatement (UseDBStr);
        // selectNameValueLim1 = this.dbconn.prepareStatement (selectNameValueLim1Str);
        // selectNameValue = this.dbconn.prepareStatement (selectNameValueStr);
    }

    public <T> void save (final T objectData) {
        /**
         * С помощью рефлексии в объекте objectData надо найти поле,
         * отмеченное @Id .
         * По значению поля @Id надо проверить, если ли этот объект в
         * базе или нет.
         * Если объекта нет, надо сформировать insert, если есть
         * update.
         */
        boolean uF = false; // use update instead of insert
        String ocn = objectData.getClass().getSimpleName(),
            keyName = null, keyValue = null;
        HashMap <String, String> cvs = new HashMap <> ();
        Field[] odfs = objectData.getClass().getDeclaredFields();
        for (Field odf : odfs) {
            boolean iF = false;
            String colName, colValue;
            try {
                colName = odf.getName();
                colValue = getStrValue(objectData, odf);
                if (keyName == null) {
                    iF = isIdField (odf);
                    if (iF) {
                        keyName = colName;
                        keyValue = colValue;
                        uF = isExist (ocn, colName, colValue);
                    }
                }
                if (!iF)
                    cvs.put (colName, colValue);
            }
            catch (Exception ex) {ex.printStackTrace();}
        }
        if (keyName != null || !cvs.isEmpty())
            if (__USE_SQL_STRING)
                saveSQLize(ocn, keyName, keyValue, cvs, uF);
            else
                saveSQLtatement(ocn, keyName, keyValue, cvs, uF); // fixme!
    }

    public <T> T load (final long id, final Class<T> clazz) {
        return findBy(clazz.getSimpleName(),
                      "id", Long.toString (id), clazz);
    }

    public void close() {
        try {
            dbst.close();
            //dbconn.close();
        }
        catch (SQLException sex) {
            sex.printStackTrace();
        }
    }

    public ResultSet execQuery (final String sqlStr) {
        logger.debug(sqlStr);
        ResultSet rs = null;
        try {
            setStatement();
            if (dbst.execute(sqlStr)) {
                rs = dbst.getResultSet();
            }
        }
        catch (SQLException sex) {
            sex.printStackTrace();
        }
        return rs;
    }

    public ResultSet execQuery (final PreparedStatement statement) {
        //logger.debug (statement);
        ResultSet rs = null;
        try {
            setStatement();
            if (statement.execute()) {
                rs = statement.getResultSet();
                // logger.debug (rs.toString());
            }
        }
        catch (SQLException sex) {
            sex.printStackTrace();
        }
        return rs;
    }

    public void createNewDBTable() {
        // fixme stop-gap
        try {
            if (false) { // h2 умеет ... просто таблицы?
                PreparedStatement createDBStatement
                    = this.dbconn.prepareStatement
                    (createDBStatementStr);
                try (ResultSet rs = execQuery(createDBStatement)) {
                    ;
                }
                catch (SQLException sex) {
                    sex.printStackTrace();
                }
                useDB("Otus2018L10");
            }
            PreparedStatement createTableStatement
                = this.dbconn.prepareStatement
                (createTableStatementStr);
            try (ResultSet rs = execQuery (createTableStatement)) {
                ;
            }
            catch (SQLException sex) {
                sex.printStackTrace();
            }
        }
        catch (SQLException sex) {
            sex.printStackTrace();
        }
    }

    public void useDB(final String dbname) {
        // fixme stop-gap
        if (dbname == null)
            return;
        try {
            PreparedStatement useDB = this.dbconn.prepareStatement (useDBStr);
            useDB.setString(1, dbname);
            try (ResultSet rs = execQuery(useDB)) {
                ;
            }
            catch (SQLException sex) {
                sex.printStackTrace();
            }
        }
        catch (SQLException sex) {
            sex.printStackTrace();
        }
    }

    private boolean isIdField (final Field odf) {
        /**
         * walks over annotations of given field
         * returns true if @Id is present
         */
        if (odf == null)
            return false;
        Annotation[] fas = odf.getDeclaredAnnotations();
        for (Annotation fa: fas)
            if (fa.annotationType().equals(Otus2018L10.Id.class))
                return true;
        return false;
    }

    private void setStatement() throws SQLException {
        if (dbst == null)
            dbst = dbconn.createStatement();
    }

    private <T> String getStrValue (final T objectData, Field objField) {
        // https://docs.oracle.com/javase/1.5.0/docs/api/java/lang/Class.html#getName()
        // https://stackoverflow.com/questions/44395874/get-field-values-using-reflection
        String rs = null;
        objField.setAccessible(true);
        try {
            switch (objField.getType().getSimpleName()) {
            case "long": {
                rs = Long.toString (objField.getLong (objectData));
                break;
            }
            case "int": {
                rs = Integer.toString (objField.getInt (objectData));
                break;
            }
            default:
                rs = (String) objField.get (objectData);
            }
        }
        catch (IllegalAccessException ex) {ex.printStackTrace();}
        return rs;
    }

    private boolean isExist (final String tableName, final String keyName,
                             final String keyValue) {
        /**
         * searches object in table tableName by keyValue in column keyName
         * returns true if at least 1 such object exists
         * NB: man Formatter
         */
        boolean rF = false;
        String sqlsel = String.format (selectNameValueLim1Str,
                                       tableName, keyName);
        // logger.debug (sqlsel + " " + keyValue);
        try {
            PreparedStatement selectNameValueLim1
                = this.dbconn.prepareStatement (sqlsel);
            // (String.format (selectNameValueLim1Str, tableName, keyName));
            // selectNameValueLim1.setString (1, keyName);
            // selectNameValueLim1.setString (2, tableName);
            selectNameValueLim1.setString (1, keyValue);
            try (ResultSet rs = execQuery (selectNameValueLim1)) {
                if (rs != null) {
                    logger.debug (rs.toString());
                    rF = rs.isBeforeFirst();
                }
            }
            catch (SQLException sex) {
                sex.printStackTrace();
            }
        }
        catch (SQLException sex) {
            sex.printStackTrace();
        }
        return rF;
    }

    private <T> T findBy (final String tableName, final String keyName,
                          final String keyValue, final Class<T> claz) {
        /**
         * searches object in table tableName by keyValue in column keyName
         * return new object of claz
         * NB: man Formatter
         */
        T rT = null;
        try {
            PreparedStatement selectNameValue
                = this.dbconn.prepareStatement
                (String.format (selectNameValueStr, tableName, keyName));
            // selectNameValue.setString (1, tableName);
            // selectNameValue.setString (2, keyName);
            selectNameValue.setString (1, keyValue);
            try (ResultSet rs = execQuery(selectNameValue)) {
                if (rs != null) {
                    logger.debug (rs.toString());
                    rT = parseResultSet (rs, claz);
                }
            }
            catch (SQLException sex) {
                sex.printStackTrace();
            }
        }
        catch (SQLException sex) {
            sex.printStackTrace();
        }
        return rT;
    }

    private void saveSQLize (final String tableName,
                             final String keyName, final String keyValue,
                             final HashMap <String, String> cvs,
                             final boolean uF) {
        /**
         * prepares and executes insert|update statement
         */
        // logger.debug (String.format ("update %b\n", uF));
        // PreparedStatement stat = dbconn.perpareStatement ();
        try (ResultSet rs = execQuery
             (uF
              ? saveSQLizeUpdate (tableName, keyName, keyValue, cvs)
              : saveSQLizeInsert (tableName, keyName, keyValue, cvs))) {
            ;
        }
        catch (SQLException sex) {
            sex.printStackTrace();
        }
    }

    private String saveSQLizeInsert (final String tableName,
                            final String keyName, final String keyValue,
                            final HashMap <String, String> cvs) {
        /**
         * prepares and executes insert|update statement
         */
        // fixme! Запросы нельзя конкатенировать, их надо параметризовать через preparedStatement
        StringBuilder qstr = new StringBuilder("insert into `");
        String[] ks = new String [cvs.size() + 1],
            vs = new String [cvs.size() + 1];
        ks[0] = keyName;
        vs[0] = keyValue;
        int i = 1;
        for (String k : cvs.keySet()) {
            ks[i] = k;
            vs[i] = cvs.get(k);
            ++i;
        }
        // fixme! ks may be empty (不行 && 不是)
        qstr.append (tableName);
        qstr.append ("` (`");
        qstr.append (String.join ("`,`", ks)); // fixme
        qstr.append ("`) values ('");
        qstr.append (String.join ("','", vs)); // fixme
        qstr.append ("');");
        return qstr.toString();
    }

    private String saveSQLizeUpdate (final String tableName,
                            final String keyName, final String keyValue,
                            final HashMap <String, String> cvs) {
        /**
         * prepares and executes insert|update statement
         */
        // fixme! Запросы нельзя конкатенировать, их надо параметризовать через preparedStatement
        StringBuilder qstr = new StringBuilder("update `");
        String[] sets = new String [cvs.size()];
        qstr.append (tableName);
        qstr.append ("` set ");
        int i = 0;
        for (String k : cvs.keySet()) // `col`='val'
            sets[i++] = String.format ("`%s`='%s'", k, cvs.get(k));
        qstr.append (String.join (",", sets));
        qstr.append (" where `");
        qstr.append (keyName);
        qstr.append ("`='");
        qstr.append (keyValue);
        qstr.append ("';");
        return qstr.toString();
    }

    private <T> T parseResultSet (final ResultSet rs, Class<T> claz) {
        /** см. UserH2.fillUser
         */
        if (rs == null || claz == null)
            return null;
        T nT = null;
        try {
            // Field[] odfs = claz.getDeclaredFields();
            nT = claz.newInstance();
            // new User (rs.getLong ("id"), rs.getString ("name"), rs.getInt ("age"))
            ResultSetMetaData rmd = rs.getMetaData();
            int cols = rmd.getColumnCount();
            // if (cols < odfs.length) return null;
            String[] coln = new String [cols];
            for (int i = 1; i <= cols; ++i)
                coln[i-1] = rmd.getColumnName (i);
            for (;rs.next();) {
                int i = 0;
                for (String cn : coln) {
                    ++i;
                    logger.debug(String.format("column: %s\t %s", cn,
                                               rmd.getColumnTypeName(i)));
                    setFiled(nT, rs, cn);
                }
            }
        }
        // SQLException
        // InstantiationException
        // NoSuchFieldException
        // IllegalAccessException
        catch (Exception mex) {
            mex.printStackTrace();
        }
        return nT;
    }

    private <T> void setFiled(T ct, final ResultSet rs, final String coln) {
        try {
            // столбцы -- капсом %\
            Field[] odfs = ct.getClass().getDeclaredFields ();
            Field odf = null;
            for (Field cf : odfs)
                if (cf.getName().equalsIgnoreCase (coln)) {
                    odf = cf;
                    break;
                }
            if (odf == null) // no such field
                return;
            odf.setAccessible (true);
            logger.debug(String.format("setting '%s' to '%s'",
                                       odf.getName(), coln));
            switch(odf.getType().getSimpleName()) {
            case "Short":
            case "short" : {
                odf.set(ct, rs.getShort (coln));
                break;
            }
            case "Integer":
            case "int" : {
                odf.set(ct, rs.getInt (coln));
                break;
            }
            case "Long":
            case "long" : {
                odf.set(ct, rs.getLong (coln));
                break;
            }
            case "Float" :
            case "float" : {
                odf.set(ct, rs.getFloat (coln));
                break;
            }
            case "Double" :
            case "double" : {
                odf.set(ct, rs.getDouble (coln));
                break;
            }
            case "String" : {
                odf.set(ct, rs.getString (coln));
                break;
            }
            default:
                ;
            }
        }
        // SQLException
        // NoSuchFieldException
        // IllegalAccessException
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void saveSQLtatement (final String tableName,
                                  final String keyName, final String keyValue,
                                  final HashMap <String, String> cvs,
                                  final boolean uF) {
        /**
         * prepares and executes insert|update statement
         */
        PreparedStatement prs = null;
        if (uF)
            prs = prepareUpdate(tableName, keyName, keyValue, cvs);
        else
            prs = prepareInsert(tableName, keyName, keyValue, cvs);
        if (prs != null)
            try {
                prs.execute();
                try (ResultSet rs = prs.getResultSet()) {
                    ;
                }
                catch (SQLException sex) {
                    sex.printStackTrace();
                }
            }
            catch (SQLException sex) {
                sex.printStackTrace();
            }
    }

    private PreparedStatement prepareInsert(final String tableName,
                                            final String keyName,
                                            final String keyValue,
                                            final HashMap <String, String> cvs) {
        /**
         * prepares and returns insert statement
         * insert into `tableName` (`keyName`,`key0`,`key1`,...,`keyN`) values (keyValue,val0,val1,...,valN);
         */
        PreparedStatement prs = null;
        StringBuilder strI = new StringBuilder("insert into `");
        strI.append(tableName);
        strI.append("` (");
        String[] kvs = new String[cvs.size()];
        int i = 0;
        for(String k : cvs.keySet())
            kvs[i++] = String.format("`%s`", k);
        strI.append(String.join(",", kvs));
        strI.append(",`");
        strI.append(keyName);
        strI.append("`) values (");
        // N+1: "?,..,?"
        for(i=0; i<cvs.size(); ++i)
            strI.append("?,");
        strI.append("?)");
        try {
            prs = dbconn.prepareStatement(strI.toString());
            i = 1;
            for(String k : cvs.keySet())
                prs.setString(i++, cvs.get(k));
            prs.setString(i, keyValue);
        }
        catch (SQLException sex) {
            sex.printStackTrace();
            prs = null;
        }
        return prs;
    }

    private PreparedStatement prepareUpdate(final String tableName,
                                            final String keyName,
                                            final String keyValue,
                                            final HashMap <String, String> cvs) {
        /**
         * prepares and returns update statement
         * update tableName set `key0`=val0,`key1`=val1,...,`keyN`=valN where `keyName`=keyValue;
         */
        PreparedStatement prs = null;
        StringBuilder strU = new StringBuilder("update `");
        strU.append(tableName);
        strU.append("` set ");
        String[] kvs = new String[cvs.size()];
        int i = 0;
        for(String k : cvs.keySet())
            kvs[i++] = String.format("`%s`=?", k);
        strU.append(String.join(",", kvs));
        strU.append(" where `");
        strU.append(keyName);
        strU.append("`=?");
        try {
            prs = dbconn.prepareStatement(strU.toString());
            i = 1;
            for(String k : cvs.keySet())
                prs.setString(i++, cvs.get(k));
            prs.setString(i, keyValue);
        }
        catch (SQLException sex) {
            sex.printStackTrace();
            prs = null;
        }
        return prs;
    }

}
