// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-05-14 18:17:12 korskov>

package Otus2018L10;

import java.util.ArrayList;
import java.util.List;

import java.lang.AutoCloseable;
import java.io.Closeable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import org.h2.tools.Server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class UserH2 implements UserDAO, AutoCloseable {
    static final Logger logger = LoggerFactory.getLogger (UserH2.class);
    static final String insertTemplateStr = "insert into User (id, name, age) values (?, ?, ?);",
        updateTemplateStr = "update User set name=?, age=? where id=?;",
        createDBStatementStr = "create database if not exists Otus2018L10;",
        createTableStatementStr = "create table if not exists User (id bigint(20) NOT NULL auto_increment, name varchar(255), age int(3))",
        selectByIDTemplateStr = "select * from User where id=?;";

    private H2Connect h2inst;
    private Connection h2conn;
    private Executor h2exec;

    UserH2() {
        h2inst = null;
        h2conn = null;
        h2exec = null;
    }

    public List <User> findAll() {return null;}
    public List <User> findByName (final String uname) {return null;}
    public List <User> findByAge (final long uage) {return null;}

    public List <User> findById (final long uid) {
        /** a 1-item list, but still the list
         */
        List <User> luser = null;
        try {
            //if (h2exec == null)
            setExecutor();
            PreparedStatement selectByIDTemplate
                = h2conn.prepareStatement (selectByIDTemplateStr);
            selectByIDTemplate.setLong (1, uid);
            try (ResultSet rs = h2exec.execQuery (selectByIDTemplate)) {
                if (rs != null) {
                    luser = fillUser (rs);
                    //rs.close();
                }
            }
            catch (SQLException sex) {
                sex.printStackTrace();
            }
        }
        catch (SQLException sex) {
            sex.printStackTrace();
        }
        return luser;
    }

    public boolean insert (final User user) {
        boolean rc = false;
        try {
            //if (h2exec == null)
            setExecutor();
            PreparedStatement insertTemplate
                = h2conn.prepareStatement (insertTemplateStr);
            insertTemplate.setLong (1, user.getId());
            insertTemplate.setString (2, user.getName());
            insertTemplate.setInt (3, user.getAge());
            try (ResultSet rs = h2exec.execQuery (insertTemplate)) {
                if (rs != null) {
                    rc = true;
                    // rs.close();
                }
            }
            catch (SQLException sex) {
                sex.printStackTrace();
            }
        }
        catch (SQLException sex) {
            sex.printStackTrace();
        }
        return rc;
    }

    public boolean update (final User user) {
        boolean rc = false;
        try {
            //if (h2exec == null)
            setExecutor();
            PreparedStatement updateTemplate
                = h2conn.prepareStatement (updateTemplateStr);
            updateTemplate.setString (1, user.getName());
            updateTemplate.setInt (2, user.getAge());
            updateTemplate.setLong (3, user.getId());
            try (ResultSet rs = h2exec.execQuery (updateTemplate)) {
                if (rs != null) {
                    rc = true;
                    // rs.close();
                }
            }
            catch (SQLException sex) {
                sex.printStackTrace();
            }
        }
        catch (SQLException sex) {sex.printStackTrace();}
        return rc;
    }

    public void close() {
        try {
            if (h2exec != null)
                h2exec.close();
            if (h2conn != null)
                h2conn.close();
            if (h2inst != null)
                h2inst.close();
        }
        catch (SQLException ex) {;}
    }

    private void setH2Instance() throws SQLException {
        if (h2inst == null)
            h2inst = new H2Connect();
    }

    private void setH2Connection() throws SQLException {
        if (h2conn != null)
            return;
        if (h2inst == null)
            setH2Instance();
        h2conn = h2inst.open();
    }

    private void setExecutor() throws SQLException {
        if (h2exec != null)
            return;
        if (h2conn == null)
            setH2Connection();
        h2exec = new Executor (h2conn);
        try {
            PreparedStatement createTableStatement
                = h2conn.prepareStatement
                (createTableStatementStr);
            try (ResultSet rs = h2exec.execQuery (createTableStatement)) {
                ;
            }
            catch (SQLException sex) {
                sex.printStackTrace();
            }
        }
        catch (SQLException sex) {
            sex.printStackTrace();
        }
    }

    private List <User> fillUser (ResultSet rs) {
        if (rs == null)
            return null;
        List <User> luser = new ArrayList <> ();
        try {
            // ResultSetMetaData rsmd = rs.getMetaData();
            // System.out.printf ("cols %d\n", rsmd.getColumnCount());
            for (;rs.next();) { // set cursor to next row of result set
                luser.add
                    (new User
                     (rs.getLong ("id"),
                      rs.getString ("name"),
                      rs.getInt ("age")));
            }
        }
        catch (SQLException ex) {ex.printStackTrace();}
        return luser;
    }
}
