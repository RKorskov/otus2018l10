// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-02-12 18:51:38 korskov>

package Otus2018L10;

import java.lang.annotation.*;
import java.lang.reflect.*;
import java.lang.Class;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//import org.h2.tools.Server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Statementorum {
    static final Logger logger = LoggerFactory.getLogger (Statementorum.class);

    /**
     * SQL statement builders for databases, tables, classes, etc.
     */

    private String createNewDB() {
        /**
         * Создайте в базе таблицу User с полями:
         * 1. id bigint(20) NOT NULL auto_increment;
         * 2. name varchar(255);
         * 3. age int(3);
         * https://dev.mysql.com/doc/refman/8.0/en/create-table.html
         */
        //createTable (User.class)
        final String CDB = "create database if not exists test16;",
            CTU = "create table if not exists test16.User (id bigint(20) not null auto_increment primary key, name varchar(255), age int(3));"; // mysql 5.7
        return CDB;
    }

    /*
    private String <T> createTableSQL (T thud) {
        return getCreateTableSQL(thud.getClass());
    }

    private String getCreateTableSQL (Class thud) {
        // returns string for table creation for given class
        Field[] odfs = objectData.getClass().getDeclaredFields();
        StringBuffer fields = new StringBuffer();
        for (Field odf : odfs) {
            switch (odf.getType().getName()) {
            case "short":
            case "int":
            case "long": {
                fields.append (odf.getName());
                fields.append (" bigint (28)"); // ceil (log(MAXVAL)/log(10))
                break;
            }
            case "float":
            case "double": {
                fields.append (odf.getName());
                fields.append (" double (35)"); // ceil (log(2^80)/log(10))
                break;
            }
            case "char":
            case "String": {
                fields.append (odf.getName());
                fields.append (" varchar (255)");
                break;
            }
            default:; // skip
            }
            Annotation[] fas = odf.getDeclaredAnnotations();
            for (Annotation fa: fas) {
                String an = fa.toString();
            }
        }
        return String.format ("create table if not exists %s (%s);",
                              thud.getSimpleName(), fields.toString());
    }
    */

}
