// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-02-11 19:30:21 korskov>

package Otus2018L10;

import java.lang.annotation.*;
import java.lang.reflect.*;
import java.lang.Class;
import java.lang.AutoCloseable;
import java.io.Closeable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.h2.tools.Server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class H2Connect implements Closeable, AutoCloseable {
    static final Logger logger = LoggerFactory.getLogger (H2Connect.class);

    Connection dbconn = null;

    private static class SingletonH2Connect implements
                                                Closeable, AutoCloseable {
        // http://zetcode.com/java/h2database/
        //final static String H2_URL = "jdbc:h2:tcp://localhost/tmp/h2test";
        final static String H2_URL = "jdbc:h2:mem:";
        final static String H2_USER = "h2root";

        final static SingletonH2Connect h2connectInstance =
            new SingletonH2Connect();
        private Connection h2con = null;

        public static Connection open() throws SQLException {
            return open (H2_URL);
        }

        public static Connection open (final String url) throws SQLException {
            if (h2connectInstance.h2con == null)
                h2connectInstance.h2con = DriverManager.getConnection (url);
            return h2connectInstance.h2con;
        }

        public void close() {
            try {
                if (h2connectInstance != null)
                    h2connectInstance.h2con.close();
            }
            catch (SQLException ex) {;}
        }
    }

    /** http://www.h2database.com/html/quickstart.html
        http://zetcode.com/java/h2database/
     */
    H2Connect() throws SQLException {
        this.dbconn = SingletonH2Connect.open();
    }

    public Connection open() throws SQLException {
        if (this.dbconn == null)
            this.dbconn = SingletonH2Connect.open();
        return dbconn;
    }

    public void close() {
        SingletonH2Connect.h2connectInstance.close();
    }

}
